"""MainForm plugins package."""

from pineboolib.plugins.mainform.eneboo import eneboo  # noqa: F401
from pineboolib.plugins.mainform.eneboo_mdi import eneboo_mdi  # noqa: F401
