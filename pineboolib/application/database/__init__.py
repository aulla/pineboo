"""
Database package to manage data.

This package holds all functions and classes to manage the application data.
"""

from pineboolib.application.database import pnsignals

DB_SIGNALS = pnsignals.PNSignals()
