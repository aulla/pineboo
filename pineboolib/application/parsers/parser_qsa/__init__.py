"""
Parse QS files and write Python files.
"""

USE_THREADS: bool = True
PARSER_QSA_VERSION: str = "1.9"
IGNORE_NO_PYTHON_TAGS: bool = False
STRICT_MODE: bool = False
