# -*- coding: utf-8 -*-
"""
Base module for the execution of pineboo.

Library oriented to emulate Eneboo from python.
"""
from pineboolib.core.utils import logging  # noqa: F401
