"""
Interfaces module.

This module contains some common class interfaces, so they can be interchanged.
"""

from pineboolib.interfaces.ifieldmetadata import IFieldMetaData  # noqa: F401
from pineboolib.interfaces.itablemetadata import ITableMetaData  # noqa: F401
from pineboolib.interfaces.imanager import IManager  # noqa: F401
