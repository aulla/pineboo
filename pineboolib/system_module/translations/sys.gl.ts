<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
  <context>
    <name>Dialog</name>
    <message>
      <location filename="../../application/staticloader/ui/static_loader.ui" line="0" />
      <source>Dialog</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../application/staticloader/ui/static_loader.ui" line="0" />
      <source>Directorios de búsqueda</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/ui/static_loader.ui" line="0" />
      <source>NO DATABASE</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>DlgConnect</name>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.py" line="392" />
      <source>Carpeta profiles</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Acceso</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Perfil :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Borra el perfil seleccionado</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Edita el perfil seleccionado</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Contraseña :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Entrar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Nuevo Perfil</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Descripción :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Base de Datos :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Tipo Servidor :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Url Servidor :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Puerto Servidor :</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Usuario BD :</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Contraseña BD :</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Rep. Contraseña BD :</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Rep. Contraseña :</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Acceso sin contraseña</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Guardar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Acerca de</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Pico Eneboo Core. 2018</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Desarrollado por la comunidad Eneboo
 basandose en el proyecto pineboo de:
 David Martínez Martí</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>https://github.com/deavid/pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Licencia</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Copyright © 2012-2019 David Martínez Martí

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../loader/dlgconnect/dlgconnect.ui" line="0" />
      <source>Ruta profiles :</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLFieldDB</name>
    <message>
      <location filename="../../fllegacy/flfielddb.py" line="3121" />
      <source>Elegir archivo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flfielddb.py" line="3676" />
      <source>Error: fieldName vacio</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLFormDB</name>
    <message>
      <location filename="../../fllegacy/flformdb.py" line="614" />
      <source>No hay metadatos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformdb.py" line="650" />
      <location filename="../../fllegacy/flformdb.py" line="647" />
      <source>Exportar a XML(F3)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformdb.py" line="645" />
      <source>F3</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformdb.py" line="667" />
      <source>F8</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformdb.py" line="698" />
      <source>Esc</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLFormRecordDB</name>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="242" />
      <source>F3</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="354" />
      <location filename="../../fllegacy/flformrecorddb.py" line="260" />
      <source>F8</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="287" />
      <source>F5</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="308" />
      <source>F6</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="331" />
      <source>F7</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="373" />
      <source>F9</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="399" />
      <source>F10</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformrecorddb.py" line="423" />
      <source>Esc</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLFormSearchDB</name>
    <message>
      <location filename="../../fllegacy/flformsearchdb.py" line="150" />
      <source>F3</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flformsearchdb.py" line="168" />
      <source>F8</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLSerialPort</name>
    <message>
      <location filename="../../fllegacy/flserialport.py" line="82" />
      <source>Opción deshabilitada</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flserialport.py" line="83" />
      <source>FLSerialPort no está disponible para IOS</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLSmtpClient</name>
    <message>
      <location filename="../../fllegacy/flsmtpclient.py" line="311" />
      <source>Componiendo mensaje</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>FLTableDB</name>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1535" />
      <source>Campo,Condición,Valor,Desde,Hasta</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1597" />
      <location filename="../../fllegacy/fltabledb.py" line="1589" />
      <location filename="../../fllegacy/fltabledb.py" line="1537" />
      <source>Todos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1602" />
      <location filename="../../fllegacy/fltabledb.py" line="1538" />
      <source>Contiene Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1603" />
      <location filename="../../fllegacy/fltabledb.py" line="1539" />
      <source>Empieza por Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1604" />
      <location filename="../../fllegacy/fltabledb.py" line="1540" />
      <source>Acaba por Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1598" />
      <location filename="../../fllegacy/fltabledb.py" line="1590" />
      <location filename="../../fllegacy/fltabledb.py" line="1541" />
      <source>Igual a Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1599" />
      <location filename="../../fllegacy/fltabledb.py" line="1591" />
      <location filename="../../fllegacy/fltabledb.py" line="1542" />
      <source>Distinto de Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1605" />
      <location filename="../../fllegacy/fltabledb.py" line="1543" />
      <source>Mayor que Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1606" />
      <location filename="../../fllegacy/fltabledb.py" line="1544" />
      <source>Menor que Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1607" />
      <location filename="../../fllegacy/fltabledb.py" line="1545" />
      <source>Desde - Hasta</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1600" />
      <location filename="../../fllegacy/fltabledb.py" line="1592" />
      <location filename="../../fllegacy/fltabledb.py" line="1546" />
      <source>Vacío</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="1601" />
      <location filename="../../fllegacy/fltabledb.py" line="1593" />
      <location filename="../../fllegacy/fltabledb.py" line="1547" />
      <source>No Vacío</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="2696" />
      <source>Opción deshabilitada</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="2697" />
      <source>Esta opción ha sido deshabilitada.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="2774" />
      <source>Sí</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/fltabledb.py" line="2774" />
      <source>No</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>Form</name>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Acerca de pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Principal</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Pineboo es un port del motor AbanQ/Eneboo a PyQt6, mantenido por la comunidad Eneboo basandose en el proyecto pineboo de: David Martínez Martí

https://github.com/deavid/pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Empresas colaboradoras:</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>José A. Fdez Fdez (aullasistemas@gmail.com)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>YeboYebo S.L. (mail@yeboyebo.es)</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Miguel J. (miguelajsmaps@gmail.com) *Documentación</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Librerías</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Copiar al portapapeles</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Licencia</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Copyright © 2012 David Martínez Martí

Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the “Software”), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR 
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/dlg_about/about_pineboo.ui" line="0" />
      <source>Cerrar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <location filename="../forms/fltest.ui" line="0" />
      <location filename="../forms/flusers.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flusers.ui" line="0" />
      <source>iduser</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flusers.ui" line="0" />
      <source>idgroup</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <location filename="../forms/flusers.ui" line="0" />
      <source>descripcion</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>id</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>string_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>date_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>time_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>double_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>bool_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>uint_field</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/fltest.ui" line="0" />
      <source>bloqueo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <location filename="../forms/flmodulos.ui" line="0" />
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>idmodulo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>version</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>icono</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>idarea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>Ficheros</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>flfiles</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flmodulos.ui" line="0" />
      <source>Log</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>MainForm</name>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="112" />
      <source>Cascada</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="119" />
      <source>Mosaico</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="126" />
      <source>Cerrar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="140" />
      <source>Salir</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="143" />
      <source>Ctrl+Q</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="151" />
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="150" />
      <source>Salir de la aplicación (Ctrl+Q)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="228" />
      <source>Barra de Herramientas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="237" />
      <source>Barra de Estado</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="253" />
      <source>&amp;Ver</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="258" />
      <source>&amp;Módulos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="372" />
      <source>Carga Estática desde Disco Duro</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="392" />
      <source>Reiniciar Script</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="991" />
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="412" />
      <source>Mostrar Consola de mensajes</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="468" />
      <source>Fuente</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="479" />
      <source>Estilo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="490" />
      <source>Indice</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="501" />
      <source>Acerca de Pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="512" />
      <source>Visita Eneboo.org</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="523" />
      <source>Acerca de Qt</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="1099" />
      <source>&amp;Ventana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo_mdi/eneboo_mdi.py" line="1149" />
      <source>Listo.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="735" />
      <source>Añadir Marcadores</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="758" />
      <source>Eliminar Marcador</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="917" />
      <source>Menú</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="966" />
      <source>Configurar carga estática</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="980" />
      <source>Recargar scripts</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1002" />
      <source>&amp;Salir</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1086" />
      <source>Marcadores</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1088" />
      <source>Recientes</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1090" />
      <source>Módulos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1096" />
      <source>&amp;Vistas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/eneboo.py" line="1237" />
      <source>Mas</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>MetaData</name>
    <message>
      <location filename="../../fllegacy/tests/test_systype.py" line="143" />
      <source>123</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>PNAccessControlLists</name>
    <message>
      <location filename="../../application/acls/pnaccesscontrollists.py" line="90" />
      <source>Lista de control de acceso errónea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/acls/pnaccesscontrollists.py" line="116" />
      <source>Lista de control de acceso cargada</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>PNApplication</name>
    <message>
      <location filename="../../application/pnapplication.py" line="383" />
      <source>Se han detectado transacciones abiertas en estado inconsistente.
Esto puede suceder por un error en la conexión o en la ejecución
de algún proceso de la aplicación.
Para mantener la consistencia de los datos se han deshecho las
últimas operaciones sobre la base de datos.
Los últimos datos introducidos no han sido guardados, por favor
revise sus últimas acciones y repita las operaciones que no
se han guardado.
</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/pnapplication.py" line="669" />
      <source>Mensajes de Eneboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/pnapplication.py" line="807" />
      <source>Salir ...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/pnapplication.py" line="808" />
      <source>¿ Quiere salir de la aplicación ?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/pnapplication.py" line="977" />
      <source>Elegir archivo</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>PNSqlCursor</name>
    <message>
      <location filename="../../application/database/pnsqlcursor.py" line="2330" />
      <location filename="../../application/database/pnsqlcursor.py" line="770" />
      <source>No hay ningún registro seleccionado</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/database/pnsqlcursor.py" line="782" />
      <source>El registro activo será borrado. ¿ Está seguro ?</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/database/pnsqlcursor.py" line="824" />
      <location filename="../../application/database/pnsqlcursor.py" line="790" />
      <source>Aviso</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>PNStaticLoader</name>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="178" />
      <source>Carpeta</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="178" />
      <source>Activo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="255" />
      <source>Selecciones el directorio a insertar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="290" />
      <source>Selecciones el directorio a modificar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="315" />
      <source>Borrar registro</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/staticloader/pnmodulesstaticloader.py" line="316" />
      <source>El registro activo será borrado. ¿ Está seguro ?</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>Pineboo</name>
    <message>
      <location filename="../../fllegacy/flfielddb.py" line="2661" />
      <source>Guardar imagen como</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flfielddb.py" line="2670" />
      <source>Error</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flfielddb.py" line="2671" />
      <source>Error guardando fichero</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>QHttp</name>
    <message>
      <location filename="../../q3widgets/qhttp.py" line="375" />
      <source>Unknown error</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>TestFLTableDB</name>
    <message>
      <location filename="../../fllegacy/tests/test_fltabledb.py" line="261" />
      <source>Igual a Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/tests/test_fltabledb.py" line="307" />
      <location filename="../../fllegacy/tests/test_fltabledb.py" line="295" />
      <location filename="../../fllegacy/tests/test_fltabledb.py" line="272" />
      <source>Contiene Valor</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/tests/test_fltabledb.py" line="283" />
      <source>Distinto de Valor</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>application</name>
    <message>
      <location filename="../../plugins/sql/flqpsql.py" line="313" />
      <source>Comprobando indices</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/flmanager.py" line="1282" />
      <source>No se ha podido crear los metadatatos para la tabla %s</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../interfaces/isqldriver.py" line="360" />
      <source>Sí</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="788" />
      <source>Reestructurando registros para %s...</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="793" />
      <source>Recogiendo datos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="833" />
      <source>Regenerando datos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1020" />
      <source>Revisando tablas fllarge</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1071" />
      <source>Limpiando tablas fllarge</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1108" />
      <source>Borrando backups</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1160" />
      <location filename="../../interfaces/isqldriver.py" line="1137" />
      <source>Comprobando base de datos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1161" />
      <source>Borrando flmetadata</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1164" />
      <source>Borrando flvar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../interfaces/isqldriver.py" line="1170" />
      <source>Vacunando base de datos</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>ebcomportamiento</name>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Opciones de comportamiento</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Locales</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Controles</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>FLTable</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Estas opciones permiten modificar el comportamiento del control FLTableDB. Es muy aconsejable para que el módulo de control de acceso no tenga vulnerabilidaddes</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Desactivar edición con doble click.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Desactivar atajos de teclado.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Desactivar exportar a hoja de cálculo.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>FLField</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Resolución máxima de imágenes(default 600)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Color de campos obligatorios</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Cambiar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Autocompletar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Nunca</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Bajo Demanda (F4)</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Siempre</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Menú de acciones</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Mostrar menú de acciones reducido (sin acciones de la barra de herramientas).</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Modo de trabajo</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Usar en modo DBAdmin. Habilita menú de sistema y funciones en la gestión de tablas.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>SQL</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Usar pre ping para conexión segura.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Inicio</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Ejecutar estas funciones</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Desarrollo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Activar modo desarrollo</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Carga Estática</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Mostrar carga estática en consola.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>QSA</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Parsear todo el QSA al iniciar.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Sobreescribir caché siempre.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Convertir a Python los script QSA al cargar un módulo en la BD.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>QT3</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>No usar el spacer en modo legacy (Abanq/Eneboo).</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>KUGAR</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Habilitar modo debug.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Otros</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Eliminar carpeta caché al iniciar.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Forzar modo Mobile.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Snapshot</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Mostrar botón para capturar pantalla en formularios.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Traducciones</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Usar traducciones QM.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Caché</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Ficheros</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Carpeta temporales:</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Cam&amp;biar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Alt+B</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>No borrar caché común al iniciar.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Imágenes</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>No cachear Imágenes.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Globales</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Datos Vertical</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Descripción (acepta HTML)</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Mostrar información de conexión en la barra de tareas.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Usar múltiples tablas FLLarge (modo AbanQ).</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>&amp;Guardar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Alt+G</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>&amp;Cancelar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/ebcomportamiento.ui" line="0" />
      <source>Alt+C</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>flfiles</name>
    <message>
      <location filename="../forms/flfiles.ui" line="0" />
      <source>Ficheros</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flfiles.ui" line="0" />
      <source>nombre</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flfiles.ui" line="0" />
      <source>sha</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flfiles.ui" line="0" />
      <source>Contenido</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>formAreas</name>
    <message>
      <location filename="../forms/flareas.ui" line="0" />
      <source>Areas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flareas.ui" line="0" />
      <source>idarea</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flareas.ui" line="0" />
      <source>descripcion</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>formGroups</name>
    <message>
      <location filename="../forms/flgroups.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flgroups.ui" line="0" />
      <location filename="../forms/flgroups.ui" line="0" />
      <location filename="../forms/flgroups.ui" line="0" />
      <source>idgroup</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flgroups.ui" line="0" />
      <source>descripcion</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flgroups.ui" line="0" />
      <source>Miembros</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/flgroups.ui" line="0" />
      <source>flusers</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>mainwindow</name>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>Pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>&amp;Fuente</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>Acerca de Qt</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>Acerca de Pineboo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>Visita Eneboo.org</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>Indice</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <location filename="../../plugins/mainform/eneboo_mdi/mainform.ui" line="0" />
      <source>perune</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <source>Tab 1</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <source>&amp;Configuración</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <source>&amp;Estilo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <source>&amp;Ventana</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../plugins/mainform/eneboo/mainform.ui" line="0" />
      <source>A&amp;yuda</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>master</name>
    <message>
      <location filename="../forms/FLWidgetMasterTable.ui" line="0" />
      <location filename="../forms/master.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>master_copy</name>
    <message>
      <location filename="../forms/master_copy.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>master_print</name>
    <message>
      <location filename="../forms/master_print.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>master_print_copy</name>
    <message>
      <location filename="../forms/master_print_copy.ui" line="0" />
      <source>Form</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>python</name>
    <message>
      <location filename="../../fllegacy/tests/test_systype.py" line="47" />
      <source>hola python sin group</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>scripts</name>
    <message>
      <location filename="../scripts/flreloadbatch.py" line="97" />
      <location filename="../../plugins/dgi/dgi_qt/dgi_objects/progress_dialog_manager.py" line="19" />
      <source>Cancelar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/systype.py" line="540" />
      <source>Seleccionar Eneboo/Abanq Package</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/tests/test_systype.py" line="46" />
      <source>hola python</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../application/translator/test/test_pntranslator.py" line="22" />
      <location filename="../../fllegacy/tests/test_flutil.py" line="23" />
      <source>single</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/tests/test_flutil.py" line="24" />
      <source>variable %s</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../../fllegacy/tests/test_flutil.py" line="26" />
      <source>multiple variable %s , %s</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flloadmod.py" line="23" />
      <source>Elegir Fichero</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flmodules.py" line="214" />
      <location filename="../scripts/flloadmod.py" line="31" />
      <source>Imposible cargar el módulo.
Licencia del módulo no aceptada.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flloadmod.py" line="57" />
      <source>Este módulo depende del módulo </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flloadmod.py" line="59" />
      <source>, que no está instalado.
FacturaLUX puede fallar por esta causa.
¿Desea continuar la carga?</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadlast.py" line="21" />
      <source>Módulo a cargar (*.mod)</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadlast.py" line="22" />
      <source>Módulo a cargar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flreloadlast.py" line="51" />
      <source>Error en la carga del fichero xml .mod</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadlast.py" line="94" />
      <source>Error al crear el área:
</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadbatch.py" line="19" />
      <source>Directorio de Módulos</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadbatch.py" line="34" />
      <source>Error al buscar los módulos en el directorio:
</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadbatch.py" line="52" />
      <source>Error al cargar el módulo:
</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flreloadbatch.py" line="96" />
      <source>Aceptar</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flreloadbatch.py" line="99" />
      <source>Seleccione módulos a cargar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="59" />
      <source>- Cargando :: </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="73" />
      <source>- Actualizando :: </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="82" />
      <source>- Backup :: </source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="107" />
      <source>Convirtiendo %s a kut</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="115" />
      <source>Volcando a disco </source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flmodules.py" line="119" />
      <source>Error de conversión</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="171" />
      <location filename="../scripts/flmodules.py" line="164" />
      <source>Elegir Directorio</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="194" />
      <source>Acuerdo de Licencia.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flmodules.py" line="199" />
      <source>Sí, acepto este acuerdo de licencia.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="200" />
      <source>No, no acepto este acuerdo de licencia.</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../scripts/flmodules.py" line="251" />
      <source>* Carga finalizada.</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../scripts/flmodules.py" line="417" />
      <source>* Exportación finalizada.</source>
      <translation type="unfinished" />
    </message>
  </context>
  <context>
    <name>sys</name>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>MainWindow</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>&amp;Principal</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>&amp;Usuarios</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>toolBar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <location filename="../forms/sys.ui" line="0" />
      <source>Opciones de comportamiento</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+P</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Cargar pa&amp;quete de módulos</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Cargar paquete de módulos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+Q</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>&amp;Cargar Módulo</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Cargar Módulo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+C</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <location filename="../forms/sys.ui" line="0" />
      <source>Cargar Directorio de Módulos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+B</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Recargar Ú&amp;ltimo Módulo</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Recargar Último Módulo</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+L</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>A&amp;reas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Areas</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+R</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>&amp;Módulos</source>
      <translation type="unfinished" />
    </message>
    <message encoding="UTF-8">
      <location filename="../forms/sys.ui" line="0" />
      <source>Módulos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+M</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Regenerar Base de Dat&amp;os</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Regenerar Base de Datos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+O</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <location filename="../forms/sys.ui" line="0" />
      <source>Reiniciar</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+H</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Co&amp;pia de Seguridad</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Copia de Seguridad</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>U&amp;suarios</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Usuarios</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+S</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>&amp;Grupos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Grupos</source>
      <translation type="unfinished" />
    </message>
    <message>
      <location filename="../forms/sys.ui" line="0" />
      <source>Ctrl+G</source>
      <translation type="unfinished" />
    </message>
  </context>
</TS>
